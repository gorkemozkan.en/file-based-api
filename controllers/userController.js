const fs = require('fs')
const dataResourse = (fs.readFileSync(`${__dirname}/../data/users.json`, 'utf8'));
const data = JSON.parse((dataResourse))

exports.getUsers =  (req, res) => {
    res.status(200).json({
        status : "success",
        data: {
            companies: (data)
        }
    })
}


exports.getUser = (req, res) => {
    const result = data.filter((el)=> {
        return el.id === req.params.id*1
    })
    if(data.length<req.params.id*1) {
        res.status(404).json({
            status : "Failed !",
            message : "Use not found !"
        })
    }else {
        res.status(200).json({
            status : "Success !",
            result
        })
    }
}


exports.createUser = (req,res)=> {
    const newID = data[data.length-1].id +1
    const newUser = Object.assign({id:newID ,user:req.body})
    console.log(newUser)
    data.push(newUser)
    fs.writeFile('/data/users.json', JSON.stringify(data), function (err) {
        res.status(200).json({
            status : "Sucess !",
            data : {
                newUser : newUser
            }
        })
    })
}


exports.deleteUser = (req,res)=> {
    res.status(200).json({
        status: "Success",
        message : "User has been deleted!"
    })
}


exports.updateUser = (req,res) => {
    res.status(200).json({
        status: "Success",
        message : "User has been updated!"
    })
}