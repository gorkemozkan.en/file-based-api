const express = require('express')
const router = express.Router()
const controller = require("../controllers/userController.js");



router
    .route('/')
    .get(controller.getUsers)
    .post(controller.createUser)

router
    .route('/:id')
    .patch(controller.updateUser)
    .delete(controller.deleteUser)
    .patch(controller.updateUser)

module.exports = router;